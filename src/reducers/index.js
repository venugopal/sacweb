import {combineReducers} from "redux";
import { reducer as formReducer } from "redux-form";
import { reducer as notifReducer } from 'redux-notifications';

const rootReducer = combineReducers({
    form: formReducer,
    notifs: notifReducer,
});

export default rootReducer;
