import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/lib/Modal';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import '../styles/signUpModal.css';

// eslint-disable-next-line react/prefer-stateless-function
class SignUpModal extends React.PureComponent {
  render() {
    const { show, onHide } = this.props;
    return (
      <Modal id="SignUpModal" bsSize="large" show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title id="signUpTitle">Create Account for SAC</Modal.Title>
        </Modal.Header>
        <Modal.Body id="signUpBody">
          <Grid>
            <Row id="fname-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="fname-label" for="fname">
                  First Name
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="text"
                  name="user.fname"
                  id="userfname"
                  title="First Name"
                  value=""
                />
              </Col>
            </Row>
            <Row id="lname-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="lname-label" for="lname">
                  Last Name
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="text"
                  name="user.lname"
                  id="userlname"
                  title="Last Name"
                  value=""
                />
              </Col>
            </Row>
            <Row id="email-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="signupemail-label" for="email">
                  Email
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="email"
                  name="user.email_address"
                  id="signupemail"
                  title="Email"
                  value=""
                  placeholder="email@yourdomain.com"
                />
              </Col>
            </Row>
            <Row id="phone-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="phone-label" for="email">
                  Phone number
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="number"
                  name="user.phone"
                  id="phone"
                  title="Phone Number"
                  value=""
                />
              </Col>
            </Row>
            <Row id="password-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="signuppassword-label" for="email">
                  Password
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="password"
                  name="user.password"
                  id="loginpassword"
                  title="Password"
                  value=""
                />
              </Col>
            </Row>
            <Row id="password-row" className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="signuppassword-label" for="email">
                  Re-Enter Password
                </label>
              </Col>
              <Col xs={6}>
                {' '}
                <input
                  type="password"
                  name="user.password"
                  id="loginpassword"
                  title="Password"
                  value=""
                />
              </Col>
            </Row>
            <p id="sacdiscalimer">
              By creating an SAC account, I agree to the Rewards{' '}
              <a>Terms & Conditions</a> and to receive email notifications at
              the email address I provided.
            </p>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={this.props.onHide}>
            Create Account
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

SignUpModal.propTypes = {
  onHide: PropTypes.func,
  show: PropTypes.bool
};

export default SignUpModal;
