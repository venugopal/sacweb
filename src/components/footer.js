import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Footer extends Component{
  constructor() {
    super();
  }
  render() {
    return (
      <div>
        <p>2018 © All Rights Reserved by SAC.</p>
      </div>
    );
  }
}
export default Footer;
