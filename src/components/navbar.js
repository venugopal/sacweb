import React, { Component } from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logo from '../images/sac_logo.jpg';
import '../styles/navbar.css';
import LoginModal from './LoginModal';
import SignUpModal from './SignUpModal';

class NavBar extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showLoginModal: false,
      showSignUpModal: false
    };
  }

  openCloseLoginModal = () => {
    this.setState(prevState => ({ showLoginModal: !prevState.showLoginModal }));
  };

  openCloseSignUpModal = () => {
    this.setState(prevState => ({
      showSignUpModal: !prevState.showSignUpModal
    }));
  };

  render() {
    return (
      <div>
        <Navbar collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">
                <img src={logo} className="App-logo" alt="logo" />
              </Link>
            </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem
                title="Home"
                eventKey={1}
                componentClass={Link}
                href="/"
                to="/"
              >
                Home
              </NavItem>
              <NavDropdown
                title="Programs"
                eventKey={2}
                id="basic-nav-dropdown"
              >
                <MenuItem
                  title="Hostel"
                  eventKey={2.1}
                  componentClass={Link}
                  href="#"
                  to="#"
                >
                  Hostel Program
                </MenuItem>
                <MenuItem
                  title="Village"
                  eventKey={2.2}
                  componentClass={Link}
                  href="#"
                  to="#"
                >
                  Village Program
                </MenuItem>
                <MenuItem
                  title="Blood"
                  eventKey={2.3}
                  componentClass={Link}
                  href="#"
                  to="#"
                >
                  Blood Program
                </MenuItem>
                <MenuItem
                  title="Health Camps"
                  eventKey={2.4}
                  componentClass={Link}
                  href="#"
                  to="#"
                >
                  Health Camps
                </MenuItem>
                <MenuItem divider />
                <MenuItem
                  title="Other"
                  eventKey={2.5}
                  componentClass={Link}
                  href="#"
                  to="#"
                >
                  Other link
                </MenuItem>
              </NavDropdown>
              <NavItem
                title="Events"
                eventKey={3}
                componentClass={Link}
                href="/events"
                to="/events"
              >
                Events
              </NavItem>
              <NavItem
                title="Gallery"
                eventKey={4}
                componentClass={Link}
                href="/gallery"
                to="/gallery"
              >
                Gallery
              </NavItem>
              <NavItem
                title="About"
                eventKey={5}
                componentClass={Link}
                href="/about"
                to="/about"
              >
                About Us
              </NavItem>
              <NavItem
                title="Contact"
                eventKey={6}
                componentClass={Link}
                href="/contactus"
                to="/contactus"
              >
                Contact Us
              </NavItem>
            </Nav>
            <Nav pullRight>
              <NavItem
                title="Login"
                eventKey={1}
                componentClass={Link}
                href="#"
                onClick={this.openCloseLoginModal}
                to="/"
              >
                Login
              </NavItem>
              <NavItem
                title="Sign Up"
                eventKey={1}
                componentClass={Link}
                href="/"
                onClick={this.openCloseSignUpModal}
                to="/"
              >
                Sign Up
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <LoginModal
          show={this.state.showLoginModal}
          onHide={this.openCloseLoginModal}
        />
        <SignUpModal
          show={this.state.showSignUpModal}
          onHide={this.openCloseSignUpModal}
        />
      </div>
    );
  }
}
export default NavBar;
