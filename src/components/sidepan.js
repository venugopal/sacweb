import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import '../styles/social.css';
import '../styles/sidepan.css';

class Sidepan extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div>
        <ButtonToolbar>
          <ButtonGroup vertical block>
            <Button className="btn-custom">
              <b>Be a Blood Donor</b>
            </Button>
            <Button className="btn-custom2">
              <b>Post an Idea</b>
            </Button>
            <Button className="btn-custom">
              <b>Join Your Hands</b>
            </Button>
            <Button className="btn-custom2">
              <b>Donate to SAC</b>
            </Button>
          </ButtonGroup>
        </ButtonToolbar>
        <br />

        <Link href="#" to="/facebook" class="facebook">
          <i class="fa fa-facebook" />
        </Link>
        <Link href="#" to="#" class="youtube">
          <i class="fa fa-youtube" />
        </Link>
        <Link href="#" to="#" class="twitter">
          <i class="fa fa-twitter" />
        </Link>
      </div>
    );
  }
}
export default Sidepan;
