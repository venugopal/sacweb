import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/lib/Modal';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import '../styles/loginModal.css';

// eslint-disable-next-line react/prefer-stateless-function
class LoginModal extends React.PureComponent {
  render() {
    const { show, onHide } = this.props;
    return (
      <Modal id="loginModal" bsSize="large" show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title id="loginTitle">SAC Member Login</Modal.Title>
        </Modal.Header>
        <Modal.Body id="loginBody">
          <Grid>
            <Row className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="email-label" for="email">
                  Email
                </label>
              </Col>
              <br />
              <Col xs={6}>
                {' '}
                <input
                  type="email"
                  name="user.email_address"
                  id="loginemail"
                  title="Email"
                  value=""
                  placeholder="email@yourdomain.com"
                />
              </Col>
            </Row>
            <Row className="show-grid">
              <Col xs={2}>
                {' '}
                <label id="password-label" for="email">
                  Password
                </label>
              </Col>
              <br />
              <Col xs={6}>
                {' '}
                <input
                  type="password"
                  name="user.password"
                  id="loginpassword"
                  title="Password"
                  value=""
                />
              </Col>
            </Row>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Cancel</Button>
          <Button bsStyle="primary" onClick={this.props.onHide}>
            Login
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

LoginModal.propTypes = {
  onHide: PropTypes.func,
  show: PropTypes.bool
};

export default LoginModal;
