import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Carousel, Image } from 'react-bootstrap';

class Banner extends Component{
  constructor() {
    super();
  }
  render() {
    return (
      <Carousel>
        <Carousel.Item>
          <Image src="assests/banner1.jpg" className="Banner-image" alt="banner1"/>
          <Carousel.Caption>
            <h3>First slide label</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src="assests/banner2.jpg" className="Banner-image" alt="banner2"/>
          <Carousel.Caption>
            <h3>Second slide label</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src="assests/banner3.jpg" className="Banner-image" alt="banner3"/>
          <Carousel.Caption>
            <h3>Third slide label</h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    );
  }
}
export default Banner;
