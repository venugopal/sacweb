import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Jumbotron, Grid, Row, Col, Image, Button } from 'react-bootstrap';
import '../styles/home.css';
import Banner from './banner';
import Sidepan from './sidepan';

export default class Home extends Component {
  render() {
    return (
      <Grid className="grid-custom">
          <Row>
            <Col lg={10}>
              <Banner />
            </Col>
            <Col lg={2}>
              <div><Sidepan /></div>
            </Col>
          </Row>
          <Row className="show-grid text-center">
            <Col xs={12} sm={3} className="profile-wrapper">
              <h3>Latest Updates</h3>
              <Image src="assests/program-1.jpg" circle className="profile-pic"/>

              <p>Helping hands are better than praying lips. Don't wait for leaders do it alone from person to person.'</p>
            </Col>
            <Col xs={12} sm={3} className="profile-wrapper">
              <h3>Upcoming Events</h3>
              <Image src="assests/program-3.jpg" circle className="profile-pic"/>
              <p>Helping hands are better than praying lips. Don't wait for leaders do it alone from person to person.'</p>
            </Col>
            <Col xs={12} sm={3} className="profile-wrapper">
              <h3>Achievements</h3>
              <Image src="assests/program-5.jpg" circle className="profile-pic"/>
              <p>Helping hands are better than praying lips. Don't wait for leaders do it alone from person to person.'</p>
            </Col>
            <Col xs={12} sm={3} className="profile-wrapper">
              <h3>Messages from Desk</h3>
              <Image src="assests/program-6.jpg" circle className="profile-pic"/>
              <p>Helping hands are better than praying lips. Don't wait for leaders do it alone from person to person.'</p>
            </Col>
          </Row>
        </Grid>
    )
  }
}
