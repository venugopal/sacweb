import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import { BrowserRouter as Router } from "react-router-dom";
// import history from "./utils/historyUtils";
// import { Provider } from "react-redux";
// import store from "./store";
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(
//     <Provider store={store}>
//         <Router history={history}>
//             <App />
//         </Router>
//     </Provider>
//     , document.getElementById("root"));
registerServiceWorker();
