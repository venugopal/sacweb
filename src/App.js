import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import NavBar from './components/navbar';
import Home from './components/home';
import AboutUs from './components/navmenu/about';
import ContactUs from './components/navmenu/contact';
import Events from './components/navmenu/events';
import Gallery from './components/navmenu/gallery';
import Footer from './components/footer';

//eslint-disable-next-line

class App extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />
          <Route exact path="/" component={Home} />
          <Route path="/about" component={AboutUs} />
          <Route path="/contactus" component={ContactUs} />
          <Route path="/events" component={Events} />
          <Route path="/gallery" component={Gallery} />
          <div className='footer'><Footer /></div>
        </div>
      </Router>
    );
  }
}

export default App;
